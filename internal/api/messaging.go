package api

import (
	"encoding/json"
	"fmt"
	"github.com/cloudevents/sdk-go/v2/event"
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/entity"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/request"
	"net/url"
	"strings"
	"sync"
)

func getCredentialOfferObject(offering entity.Offering) (*entity.CredentialOfferObject, error) {
	var newCredentialOfferObject entity.CredentialOfferObject
	var rawObject []byte

	if offering.CredentialOffer != "" {
		slice := strings.Split(offering.CredentialOffer, "credential_offer=")
		unescaped, err := url.QueryUnescape(slice[len(slice)-1])
		if err != nil {
			return nil, err
		}
		rawObject = []byte(unescaped)

	} else if offering.CredentialOfferUri != "" {
		slice := strings.Split(offering.CredentialOfferUri, "credential_offer_uri=")
		unescape, err := url.QueryUnescape(slice[len(slice)-1])
		if err != nil {
			return nil, err
		}

		rawObject, err = request.Get(unescape)
		if err != nil {
			return nil, err
		}

	} else {
		return nil, fmt.Errorf("no valid credentialOffer: %v", offering)
	}
	err := json.Unmarshal(rawObject, &newCredentialOfferObject)
	if err != nil {
		return nil, fmt.Errorf("error occured while unmarshal credentialOffer: %w", err)
	}

	return &newCredentialOfferObject, nil
}

func handleOffering(event event.Event) {
	var newOffering entity.Offering
	err := json.Unmarshal(event.Data(), &newOffering)
	if err != nil {
		log.Errorf("error occured while unmarshal offering %v: %v", event, err)
	}

	log.Debugf("new offering received: %v", newOffering)

	newCredentialOfferObject, err := getCredentialOfferObject(newOffering)
	if err != nil {
		log.Errorf("error occured while getting credentialOfferObject: %v", err)
	}

	log.Debugf("credentialOfferObject retieved: %v", newCredentialOfferObject)

	isValid := checkGrantType(&newCredentialOfferObject.Grants)
	if !isValid {
		log.Errorf("credential with unsupported grant types was offered: %v", newCredentialOfferObject)
	}
}

func checkGrantType(grants *entity.Grants) bool {
	preAuthCode := grants.PreAuthorizedCode
	if preAuthCode.Code != "" {
		// is valid when pin not required
		return !preAuthCode.PinRequired
	} else {
		return false
	}
}

func startMessaging(group *sync.WaitGroup) {
	defer group.Done()
	log.Info("start messaging!")

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, config.CurrentCredentialRetrievalConfig.OfferingTopic)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := client.Close(); err != nil {
			log.Error(err)
		}
	}()

	err = client.Sub(handleOffering)
	if err != nil {
		log.Fatal(err)
	}
}
