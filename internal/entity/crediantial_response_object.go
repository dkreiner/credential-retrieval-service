package entity

type CredentialResponseObject struct {
	Format        string `json:"format"`
	Credential    string `json:"credential"`
	TransactionID string `json:"transaction_id"`
}
